mindspore_gs.ptq
=========================

Post training quantization algorithms.

.. code-block::

    import mindspore_gs.ptq as ptq

RoundToNearest Algorithm
--------------------------------

.. autosummary::
    :toctree: ptq
    :nosignatures:
    :template: classtemplate.rst

    mindspore_gs.ptq.RoundToNearest

PTQ Config
-------------

.. autosummary::
    :toctree: ptq
    :nosignatures:
    :template: classtemplate.rst

    mindspore_gs.ptq.PTQConfig

PTQMode Enum
-------------

.. autosummary::
    :toctree: ptq
    :nosignatures:
    :template: classtemplate.rst

    mindspore_gs.ptq.PTQMode
